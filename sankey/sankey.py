import os
import sys
sys.path.insert(0, os.path.pardir)

import log
import mapmyfitness as mmf

import json
import pandas as pnd


LOGGER = log.get_logger()
root_activities = "9 16".split()

trajectory_series_array = []
selected_files_path = []
city_name = 'Recife'
update_csv = False

top = os.path.join(os.path.expanduser('~'), 'DATA', 'MapMyRun', city_name)
csv_path = os.path.join(os.path.expanduser('~'), 'DATA', 'MapMyRun', city_name, city_name+'.csv')

if os.path.exists(csv_path) and not update_csv:
    trajectories = pnd.read_csv(csv_path)
else:
    for dirpath, dirnames, filenames in os.walk(top):
        # it's a target activity folder
        activity_id = dirpath.split(os.sep)[-1].split("_")[0]
        if activity_id in root_activities:
            subactivity_id = dirpath.split(os.sep)[-1].split("_")[1]
            for filename in filenames:
                if filename.endswith(".json"):
                    json_path = os.path.join(dirpath, filename)
                    _json = None
                    try:
                        _json = json.load(open(json_path))
                    except json.decoder.JSONDecodeError as jde:
                        LOGGER.info('%s', jde)
                        LOGGER.info('Skipping %s', json_path)
                        continue

                    if _json and 'time_series' in _json:
                        gpx = mmf.workout_to_gpx(_json)
                        LOGGER.info('Loaded %s', json_path)
                    else:
                        LOGGER.info('Skipping %s', json_path)
                        continue

                    keywords = json.loads(gpx.keywords)
                    user = keywords['user_id']
                    source = keywords['source']
                    time_total = keywords['active_time_total']
                    distance = float(keywords['distance_total'])
                    try:
                        start_datetime = pnd.to_datetime(keywords['start_datetime'])
                    except Exception:
                        continue

                    no_points = gpx.get_points_no()

                    trajectory_series = pnd.Series({'user_id': user,
                                                    'activity_id': activity_id,
                                                    'subactivity_id': subactivity_id,
                                                    'trajectory_id': filename.split('.')[0],
                                                    'filepath': json_path,
                                                    'source': source,
                                                    'length': distance,
                                                    'duration': time_total,
                                                    'start_time': start_datetime,
                                                    'n_points': no_points})

                    trajectory_series_array.append(trajectory_series)

    trajectories = pnd.DataFrame(trajectory_series_array)
    trajectories = trajectories.set_index('start_time')

    trajectories.to_csv(csv_path)

# Total
total_count = len(trajectories)
print()

# 2000m < distance < 43000m
min_dist, max_dist = 2000, 43000
short_dist_trajectories = trajectories[trajectories.length <= min_dist]
long_dist_trajectories = trajectories[trajectories.length >= max_dist]
distance_outliers = len(short_dist_trajectories) + len(long_dist_trajectories)
perc_distance_outliers = distance_outliers / total_count * 100
 
trajectories = trajectories[trajectories.length > min_dist]
trajectories = trajectories[trajectories.length < max_dist]
distance_inliers = len(trajectories)
perc_distance_inliers = distance_inliers / total_count * 100

print("[ 'Total ({} ~ 100%)', 'Short/Long Distance ({} ~ {:.1f}%)', {} ],".format(
    total_count, distance_outliers, perc_distance_outliers, distance_outliers))
print("[ 'Total ({} ~ 100%)', '{} ~ {:.1f}%', {} ],".format(
    total_count, distance_inliers, perc_distance_inliers, distance_inliers))


# 600s < duration < 18000s
min_dur, max_dur = 600, 18000  # 18000s = 5 hours
short_dur_trajectories = trajectories[trajectories.duration <= min_dur]
long_dur_trajectories = trajectories[trajectories.duration >= max_dur]
duration_outliers = len(short_dur_trajectories) + len(long_dur_trajectories)
perc_duration_outliers = duration_outliers / total_count * 100

trajectories = trajectories[trajectories.duration > min_dur]
trajectories = trajectories[trajectories.duration < max_dur]
duration_inliers = len(trajectories)
perc_duration_inliers = duration_inliers / total_count * 100

print("[ '{} ~ {:.1f}%', 'Short/Long Durations ({} ~ {:.1f}%)', {} ],".format(
    distance_inliers, perc_distance_inliers, duration_outliers,
    perc_duration_outliers, duration_outliers))
print("[ '{} ~ {:.1f}%', '{} ~ {:.1f}%', {} ],".format(
    distance_inliers, perc_distance_inliers, duration_inliers,
    perc_duration_inliers, duration_inliers))

# no_points < 200
min_points = 200
few_points_trajectories = trajectories[trajectories.n_points <= min_points]
points_outliers = len(few_points_trajectories)
perc_points_outliers = points_outliers / total_count * 100

trajectories = trajectories[trajectories.n_points > min_points]
points_inliers = len(trajectories)
perc_points_inliers = points_inliers / total_count * 100

print("[ '{} ~ {:.1f}%', 'Few Points ({} ~ {:.1f}%)', {}],".format(
    duration_inliers, perc_duration_inliers, points_outliers,
    perc_points_outliers, points_outliers))
print("[ '{} ~ {:.1f}%', '{} ~ {:.1f}%', {}],".format(
    duration_inliers, perc_duration_inliers, points_inliers,
    perc_points_inliers, points_inliers))

'''
# Source
android_count = len(trajectories[trajectories['source'].str.lower().str.contains("android")])
iphone_count = len(trajectories[trajectories['source'].str.lower().str.contains("iphone")])
other_count = len(trajectories) - android_count - iphone_count

perc_android = android_count/points_inliers*100
perc_iphone = iphone_count/points_inliers*100
perc_other = other_count/points_inliers*100

print("[ 'Points Inliers ({} ~ {:.1f}%)', 'Android ({} ~ {:.1f}%)', {}],".format(
    points_inliers, perc_points_inliers, android_count, perc_android , android_count))
print("[ 'Points Inliers ({} ~ {:.1f}%)', 'iPhone ({} ~ {:.1f}%)', {}],".format (
    points_inliers, perc_points_inliers, iphone_count,  perc_iphone ,iphone_count))
print("[ 'Points Inliers ({} ~ {:.1f}%)', 'Other ({} ~ {:.1f}%)', {}],".format  (
    points_inliers, perc_points_inliers, other_count,  perc_other ,other_count))
'''

'''
for name, group in groups:
    if len(group) < 5:
        trajectories = trajectories[trajectories.user_id != name]
        print(name, 'has', len(group), 'trajectories')
'''
