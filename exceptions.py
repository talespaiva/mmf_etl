class TrajectoryTooShortException(Exception):
    pass

class TrajectoryTooLongException(Exception):
    pass
