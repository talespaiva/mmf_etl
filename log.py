import logging


def get_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    if not logger.handlers:
        log_handler = logging.StreamHandler()
        log_handler.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(module)s - %(levelname)s >> %(message)s')
        log_handler.setFormatter(formatter)
        logger.addHandler(log_handler)
    return logger
