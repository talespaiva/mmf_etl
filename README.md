# MapMyFitness ETL

A suite of scripts for extracting, transforming and loading MapMyFitness user profile and workout data into json, gpx and db tables.

A `.config` file must exist at the root directory. Example:

```ini
[MapMyAPI]
BASE_URL = https://oauth2-api.mapmyapi.com/v7.1

[MapMyAPI1]
CLIENT_ID = <your client id>
CLIENT_SECRET = <your client secret>

[Postgres]
db_name = dcars
user = <username>
password = <password>
host = localhost
```

Usual flow to update database with new workouts:

1. run `mmf/get_users.py` and update `constants.py` with the output
2. run `mmf/get_workouts.py`
3. run `database/json_to_db.py`
