"""
    A script to gather user ids from MapMyRun.
    The script searches for routes inside a perimeter delimited by a radius,
    then any users that has ever recorded a workout inside this perimeter is
    returned (even if the user have many workouts outside this perimeter and
    just one workout inside the perimeter).
"""
import os
import sys
sys.path.insert(0, os.path.pardir)

import log
import constants
import mapmyfitness as mmf

LOGGER = log.get_logger()
LOGGER.info('Starting...')

ACCESS_TOKEN = mmf.authenticate()

RADIUS = 10000
city = "São Paulo"
coordinates = constants.CITIES_COORDINATES[city]

users_id = []
route_url = f'{mmf.BASE_URL}/route/?close_to_location={coordinates[0]}%2C+{coordinates[1]} \
              &maximum_distance={RADIUS}&minimum_distance=1'

total_count = None

while True:
    LOGGER.info('Requesting %s', route_url)
    route_list = mmf.get_json(route_url, ACCESS_TOKEN)

    if total_count is None:
        total_count = route_list['total_count']
        LOGGER.info('Found %s routes', total_count)

    try:
        if route_list:
            if '_embedded' in route_list and 'routes' in route_list['_embedded']:
                routes = route_list['_embedded']['routes']
                LOGGER.info('%s routes found', len(routes))
                for r in routes:
                    users_id.append(int(r['_links']['user'][0]['id']))
            else:
                LOGGER.info('route_list issue: %s', route_list)

            if '_links' in route_list and 'next' in route_list['_links']:
                route_url = str('https://oauth2-api.mapmyapi.com' +
                                route_list['_links']['next'][0]['href'])
            else:
                break

    except Exception as e:
        LOGGER.error("EXCEPTION: %s\n%s", e, route_list)
        users_id = list(set(users_id))  # remove duplicates
        users_id.sort()
        print(users_id)
        raise

users_id = list(set(users_id))  # remove duplicates
users_id.sort()
print(users_id)
