import os
import sys
sys.path.insert(0, os.path.pardir)

import log

import re
import time
import json
import requests
import webbrowser
import configparser
import urllib.parse

from os import path
from datetime import datetime, timedelta
from http.server import HTTPServer, BaseHTTPRequestHandler

import gpxpy
import gpxpy.gpx

requests.packages.urllib3.disable_warnings()

LOGGER = log.get_logger()

config = configparser.ConfigParser()
config.read(path.join(os.path.pardir, '.config'))

CLIENT_ID = config['MapMyAPI1']['CLIENT_ID']
CLIENT_SECRET = config['MapMyAPI1']['CLIENT_SECRET']
BASE_URL = config['MapMyAPI']['BASE_URL']


def authenticate():
    # As a convenience, localhost.mapmyapi.com redirects to localhost.
    redirect_uri = 'http://localhost.mapmyapi.com:12345/callback'
    authorize_url = 'https://api.ua.com/v7.1/oauth2/authorize/?' \
                    'client_id={0}&response_type=code&redirect_uri={1}'.format(
                        CLIENT_ID, redirect_uri)

    # Set up a basic handler for the redirect issued by the MapMyFitness
    # authorize page. For any GET request, it simply returns a 200.
    # When run interactively, the request's URL will be printed out.
    class AuthorizationHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            self.send_response(200, 'OK')
            self.send_header('Content-Type', 'text/html')
            self.end_headers()
            self.server.path = self.path

    parsed_redirect_uri = urllib.parse.urlparse(redirect_uri)
    server_address = parsed_redirect_uri.hostname, parsed_redirect_uri.port

    # NOTE: Don't go to the web browser just yet...
    LOGGER.info(authorize_url)
    webbrowser.open(authorize_url)

    # Start our web server. handle_request() will block until a request comes in.
    httpd = HTTPServer(server_address, AuthorizationHandler)
    LOGGER.info('Now waiting for the user to authorize the application...')
    httpd.handle_request()

    # At this point a request has been handled. Let's parse its URL.
    httpd.server_close()
    callback_url = urllib.parse.urlparse(httpd.path)
    authorize_code = urllib.parse.parse_qs(callback_url.query)['code'][0]

    access_token_url = 'https://api.ua.com/v7.1/oauth2/access_token/'
    access_token_data = {
        'grant_type': 'authorization_code',
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'code': authorize_code
    }

    response = requests.post(
        url=access_token_url, data=access_token_data, headers={'Api-Key': CLIENT_ID})

    # retrieve the access_token from the response
    try:
        access_token = response.json()
        LOGGER.info('Got an access token: %s', access_token)
        return access_token['access_token']
    except Exception:
        LOGGER.error('Did not get JSON.\n Response: %s\n Content: %s', response, response.content)


def get_json(request_url, access_token):
    max_tries = 10
    sleep_time = 30
    for i in range(1, max_tries + 1):
        try:
            response = requests.get(
                url=request_url,
                verify=False,
                headers={
                    'api-key': CLIENT_ID,
                    'authorization': 'Bearer %s' % access_token
                })
            break
        except requests.exceptions.RequestException as request_exception:
            if i == max_tries:
                raise
            LOGGER.info(request_exception)
            LOGGER.info('Request failed (attempt %s). Sleeping for %s seconds', i, sleep_time)
            time.sleep(sleep_time)

    try:
        return json.loads(response.content.decode('utf8'))
    except Exception:
        LOGGER.error('EXCEPTION: %s', response.content)


def workout_to_gpx(_json):
    gpx = gpxpy.gpx.GPX()
    gpx_track = gpxpy.gpx.GPXTrack()
    gpx.tracks.append(gpx_track)
    gpx_segment = gpxpy.gpx.GPXTrackSegment()
    gpx_track.segments.append(gpx_segment)

    created_time = _json['created_datetime']
    start_time = datetime.strptime(created_time.split('+')[0], "%Y-%m-%dT%H:%M:%S")

    point_list = _json['time_series']['position']
    for time_offset, position in point_list:
        position_values = position.values()
        if len(position_values) == 3:
            lat = position['lat']
            lng = position['lng']
            elevation = position['elevation']
        else:
            lat = position['lat']
            lng = position['lng']
            elevation = 0

        delta = timedelta(seconds=float(time_offset))
        _time = start_time + delta

        point = gpxpy.gpx.GPXTrackPoint(lat, lng, elevation=elevation, time=_time)
        gpx_segment.points.append(point)

    kw = dict()
    kw.update({"user_id": str(_json['_links']['user'][0]['id'])})
    kw.update({"trajectory_id": str(_json['_links']['self'][0]['id'])})
    kw.update({"source": str(_json['source'])})
    kw.update({"activity_type": _json['_links']['activity_type'][0]['id']})
    kw.update({"distance_total": _json['aggregates']['distance_total']})
    kw.update({"active_time_total": _json['aggregates']['active_time_total']})
    kw.update({"start_datetime": str(_json['start_datetime'])})

    gpx.keywords = json.dumps(kw)

    return gpx


def get_root_activity(code):
    """
    Searches in the activity tree for the root activity.
    :param code: activity code
    :return: root activity code
    """

    activities = json.load(open(path.join(os.path.pardir, 'MapMyFitness_Activities.json')))
    for activity in activities['_embedded']['activity_types']:
        if activity['_links']['self'][0]['id'] == code:
            return activity['_links']['root'][0]['id']


def get_activity_folder_name(code):
    """
    Return a string with RootActivityCode_ActivityCode_ActivityName
    :param code:
    :return:
    """
    activities = json.load(open(path.join(os.path.pardir, 'MapMyFitness_Activities.json')))
    for activity in activities['_embedded']['activity_types']:
        if activity['_links']['self'][0]['id'] == code:
            root = activity['_links']['root'][0]['id']

            # remove invalid windows folder characters
            activity_name = re.sub(r'[^\w\-_\. ]', '_', activity['name'])

            return '{0}_{1}_{2}'.format(root, code, activity_name)
