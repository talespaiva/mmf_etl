import os
import sys
sys.path.insert(0, os.path.pardir)

import requests
import configparser
from os import path

import log
LOGGER = log.get_logger()

config = configparser.ConfigParser()
config.read(path.join(path.pardir, '.config'))

CLIENT_ID = config['MapMyAPI2']['CLIENT_ID']
CLIENT_SECRET = config['MapMyAPI2']['CLIENT_SECRET']

# get token
access_token_url = 'https://api.ua.com/v7.1/oauth2/access_token/'
access_token_data = {
    'grant_type': 'client_credentials',
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET
}

response = requests.post(
    url=access_token_url, data=access_token_data, headers={'Api-Key': CLIENT_ID})

# retrieve the access_token from the response
token = None
try:
    access_token = response.json()
    LOGGER.info('Got an access token: %s', access_token)
    token = access_token['access_token']
except Exception:
    LOGGER.error('Did not get JSON.\n Response: %s\n Content: %s', response, response.content)

#
url = f'https://api.ua.com/v7.2/api_client/{CLIENT_ID}/'

headers = {'Authorization': f'Bearer {token}', 'Api-Key': CLIENT_ID}
LOGGER.info(headers)

key_data = requests.get(url, headers=headers)

print(key_data)
