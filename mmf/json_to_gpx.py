import os
import sys
sys.path.insert(0, os.path.pardir)

import log
import mapmyfitness as mmf

import json
import argparse

LOGGER = log.get_logger()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='Path to file or folder')
    parser.add_argument('--out', '-o', help='Path to output folder')
    args = parser.parse_args()

    if not os.path.isdir(args.path):
        raise ValueError('path must be a directory')

    out_folder = None
    if args.out and not os.path.isdir(args.out):
        os.mkdir(args.out)
        out_folder = args.out
        LOGGER.info("Folder %s created", args.out)
    else:
        path_elements = args.path.split(os.path.sep)

        if not path_elements[-1]:
            path_elements.pop()

        path_elements[-1] = path_elements[-1] + '_gpx'
        out_folder = os.path.sep + os.path.join(*path_elements) + os.path.sep

        if not os.path.isdir(out_folder):
            os.mkdir(out_folder)
            LOGGER.info("Folder %s created", out_folder)

    for root, _, filenames in os.walk(args.path):
        for filename in filenames:
            file_path = os.path.join(root, filename)
            save_path = root.replace(args.path, out_folder)
            full_gpx_path = os.path.join(save_path, '{0}.gpx'.format(filename.split('.')[0]))

            if os.path.exists(full_gpx_path):
                LOGGER.info('Skipping %s', full_gpx_path)
                continue

            if file_path.endswith('json'):
                try:
                    workout_json = json.load(open(file_path))
                    os.makedirs(os.path.dirname(full_gpx_path), exist_ok=True)
                    gpx = mmf.workout_to_gpx(workout_json)
                    temp_gpx = open(full_gpx_path, 'w+')
                    temp_gpx.write(gpx.to_xml())
                    temp_gpx.flush()
                    temp_gpx.close()
                    LOGGER.info(full_gpx_path)
                except KeyError:
                    pass
                except json.JSONDecodeError:
                    LOGGER.info('JSONDecodeError on %s', file_path)
                    raise


if __name__ == '__main__':
    main()
