"""
A script to retrieve workouts (in JSON and GPX) from a group of users.
The script creates a folder for each user and separates workouts into
subfolders according to the activity type.
The JSON file describing the user is also saved.
"""
import os
import sys
sys.path.insert(0, os.path.pardir)

import log
import mapmyfitness as mmf

import os
import json
import gzip
import shutil
from typing import List

LOGGER = log.get_logger()

ROOT_ACTIVITIES: List[str] = ['9', '16']
SOURCES: List[str] = []  # ['android', 'iphone']


def save_workouts(data_folder, user_id, workouts, access_token, skip_existing_files=True,
                  compress_files=True, create_gpx=True):
    for workout in workouts:
        activity_code = workout['_links']['activity_type'][0]['id']
        activity_folder_name = mmf.get_activity_folder_name(activity_code)

        if ROOT_ACTIVITIES:
            # Ignore if activity is different from listed in ROOT_ACTIVITIES
            root_activity = activity_folder_name.split('_')[0]
            if not activity_folder_name or root_activity not in ROOT_ACTIVITIES:
                continue

        if SOURCES:
            source = workout['source']
            if not source or not any([s for s in SOURCES if s in source.lower()]):
                continue

        if not os.path.exists(os.path.join(data_folder, str(user_id), activity_folder_name)):
            os.makedirs(os.path.join(data_folder, str(user_id), activity_folder_name))

        workout_id = workout['_links']['self'][0]['id']

        full_json_path = os.path.join(data_folder,
                                      str(user_id),
                                      activity_folder_name,
                                      '{0}.json'.format(str(workout_id)))
        full_gzip_path = os.path.join(data_folder,
                                      str(user_id),
                                      activity_folder_name,
                                      '{0}.gz'.format(str(workout_id)))

        file_path = full_gzip_path if compress_files else full_json_path
        if skip_existing_files is True and os.path.isfile(file_path):
            LOGGER.info('Skipping %s (file already exists)', file_path)
            continue

        workout_url = '{0}/workout/{1}/?field_set=time_series'.format(mmf.BASE_URL, str(workout_id))
        LOGGER.info('Retrieving %s...', workout_url)

        workout_json = mmf.get_json(workout_url, access_token)
        if not workout_json:
            LOGGER.info('workout_json = %s', workout_json)
            continue

        if 'time_series' in workout_json and 'position' in workout_json['time_series']:
            if compress_files:
                with gzip.open(full_gzip_path, 'wb') as temp_gzip:
                    json_bytes = json.dumps(workout_json).encode('utf-8')
                    temp_gzip.write(json_bytes)
                    LOGGER.info('%s created.', full_gzip_path)
            else:
                with open(full_json_path, 'w') as temp_json:
                    temp_json.write(json.dumps(workout_json, indent=4, separators=(',', ': ')))
                    LOGGER.info('%s created.', full_json_path)

            if create_gpx:
                # save GPX
                full_gpx_path = os.path.join(data_folder,
                                             str(user_id),
                                             activity_folder_name,
                                             '{0}.gpx'.format(str(workout_id)))
                temp_gpx = open(full_gpx_path, 'w')
                gpx = mmf.workout_to_gpx(workout_json)
                temp_gpx.write(gpx.to_xml())
                temp_gpx.flush()
                LOGGER.info('%s created.', full_gpx_path)
        else:
            LOGGER.info('Workout %s does NOT have position info', workout_id)


def get_workouts(city, users, access_token):
    # data_folder = 'c:\\DATA\\MapMyRun\\' + city  # windows
    # data_folder = r'~/DATA/MapMyRun/' + city  # linux
    data_folder = os.path.join(os.path.expanduser('~'), 'DATA', 'MapMyRun', city)
    LOGGER.info(data_folder)

    LOGGER.info('Found %s user(s).', len(users))
    for index, user_id in enumerate(users, 1):
        LOGGER.info('User %s (%s of %s)', user_id, index, len(users))

        '''
        try:
            subfolder_count = len(os.listdir(data_folder))
            if index < subfolder_count:
                LOGGER.info('\tskipped folder %s (%s folders already processed)',
                            user_id, subfolder_count)
                continue
        except FileNotFoundError:
            pass
        '''

        if not os.path.exists(os.path.join(data_folder, str(user_id))):
            os.makedirs(os.path.join(data_folder, str(user_id)))

        # Get information about user
        user_url = '{0}/user/{1}/'.format(mmf.BASE_URL, user_id)
        user_json = mmf.get_json(user_url, access_token)
        temp_json = open(os.path.join(data_folder, str(user_id), f'{str(user_id)}.json'), 'w')
        temp_json.write(json.dumps(user_json, indent=4, separators=(',', ': ')))
        temp_json.flush()

        workouts_url = f'{mmf.BASE_URL}/workout/?user={str(user_id)}&order_by=-start_datetime'
        has_next = True
        while has_next:
            workout_list = mmf.get_json(workouts_url, access_token)

            if workout_list:
                total_count = workout_list['total_count']
                LOGGER.info('%s workouts found', total_count)

                workouts = workout_list['_embedded']['workouts']
                save_workouts(data_folder, user_id, workouts, access_token,
                              skip_existing_files=True, compress_files=False, create_gpx=False)

                if 'next' in workout_list['_links']:
                    next_url = workout_list['_links']['next'][0]['href']
                    workouts_url = 'https://oauth2-api.mapmyapi.com' + next_url
                else:
                    has_next = False
            else:
                has_next = False

    #  delete empty folders
    LOGGER.info('Looking for empty folders to delete...')
    clear_folders(data_folder)

    LOGGER.info('FINISHED')


def clear_folders(folder):
    """
    Removes useless user folders.
    It deletes activity folders with no files inside
    and then deletes the user folder if all activities directories have been
    deleted in the first step

    :param folder: the root folder containing all user folders
    """
    for root, directories, filenames in os.walk(folder):
        if root.split(os.sep)[-1].isdigit():  # it's a user folder
            for direct in directories:
                for r, d, f in os.walk(os.path.join(root, direct)):
                    if len(f) == 0:
                        os.rmdir(r)
                        LOGGER.info("Deleted %s", r)

    for root, directories, filenames in os.walk(folder):
        if root.split(os.sep)[-1].isdigit() and len(directories) == 0:
            shutil.rmtree(root)
            LOGGER.info("Deleted %s", root)


if __name__ == '__main__':
    import constants
    ACCESS_TOKEN = mmf.authenticate()
    city = 'São Paulo'
    get_workouts(city, constants.USERS[city], ACCESS_TOKEN)
