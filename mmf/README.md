Scripts that get data from MapMyFitness public REST API.

The normal flow consists in executing `get_users.py`, updating `constants.py`, and then executing `get_workouts.py`.