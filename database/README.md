Scripts that transforms data from JSON files to the DB schema.

The normal flow is executing `json_to_db.py` and then `reverse_geocode.py`.