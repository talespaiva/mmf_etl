import os
import sys
sys.path.insert(0, os.path.pardir)

import re

import log
import db
from models import Workout, User

LOGGER = log.get_logger()


def main():
    session = db.connect_db()
    user_query = session.query(User)

    for user in user_query:
        LOGGER.info('User %s', user.id)
        workouts_query = session.query(Workout).filter_by(id_user=user.id)

        for workout in workouts_query:
            LOGGER.info('\tWorkout %s', workout.id)

            ewkt = session.scalar(workout.raw_trajectory.ST_AsEWKT())
            ewkt_points = re.split('[A-Z]+\(|,|\)', ewkt)

            new_points = []
            for point in ewkt_points:
                if len(point) > 1:
                    longitude, latitude, elevation, timestamp = point.split()
                    new_points.append(f'{longitude} {latitude} {timestamp}')

            new_ewkt = f'LINESTRINGZ({",".join(new_points)})'
            workout.temptraj = new_ewkt

        LOGGER.info('Commiting changes...')
        session.commit()

    session.close()


if __name__ == '__main__':
    main()
