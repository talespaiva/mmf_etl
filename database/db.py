import os
import sys
sys.path.insert(0, os.path.pardir)

import log
import exceptions

from os import path
import configparser

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

LOGGER = log.get_logger()

config = configparser.ConfigParser()
config.read(path.join(os.path.pardir, '.config'))

CLIENT_ID = config['MapMyAPI1']['CLIENT_ID']
CLIENT_SECRET = config['MapMyAPI1']['CLIENT_SECRET']
BASE_URL = config['MapMyAPI']['BASE_URL']


def connect_db() -> Session:
    host = config['Postgres']['host']
    db_name = config['Postgres']['db_name']
    db_user = config['Postgres']['user']
    password = config['Postgres']['password']

    engine = create_engine(f'postgresql://{db_user}:{password}@{host}/{db_name}', echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    return session


def get_one_or_create(session, model, create_method='', create_method_kwargs=None, **kwargs):
    try:
        return session.query(model).filter_by(**kwargs).one(), True
    except NoResultFound:
        try:
            kwargs.update(create_method_kwargs or {})
            created = getattr(model, create_method, model)(**kwargs)

            session.add(created)
            session.flush()
            session.commit()
            return created, False
        except IntegrityError:
            session.rollback()
            return session.query(model).filter_by(**kwargs).one(), True
        except exceptions.TrajectoryTooShortException:
            return None, False
        except exceptions.TrajectoryTooLongException:
            return None, False
