import os
import sys
sys.path.insert(0, os.path.pardir)

import log
import db
from models import Workout, User

import os
import json
import argparse

LOGGER = log.get_logger()


def main():
    # example: python json_to_db.py /home/tales/DATA/MapMyRun/Aracati/
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='Path to file or folder')
    args = parser.parse_args()

    if not os.path.isdir(args.path):
        raise ValueError('path must be a directory')

    session = db.connect_db()

    for root, _, filenames in os.walk(args.path):
        for filename in filenames:
            file_path = os.path.join(root, filename)
            LOGGER.info(file_path)

            if file_path.endswith('json'):
                try:
                    _json = json.load(open(file_path))

                    if 'created_datetime' in _json:
                        _id = str(_json['_links']['self'][0]['id'])
                        workout, exists = db.get_one_or_create(session, Workout, 'create', _json, id=_id)
                    elif 'username' in _json:
                        _id = str(_json['id'])
                        user, exists = db.get_one_or_create(session, User, 'create', _json, id=_id)
                    else:
                        unknown_user_id = filename.split('.')[0]
                        unknown_user = {'id': unknown_user_id, 'username': 'unknown'}
                        user, exists = db.get_one_or_create(session, User, 'create', unknown_user,
                                                            id=unknown_user_id)

                    if exists:
                        LOGGER.info('Entity already exists.')

                except json.JSONDecodeError:
                    LOGGER.info('JSONDecodeError on %s', file_path)
                    raise

        session.commit()
        LOGGER.info('Commited changes')

    session.close()


if __name__ == '__main__':
    main()
