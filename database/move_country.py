import os
import sys
sys.path.insert(0, os.path.pardir)

import log
import db
from models import City

LOGGER = log.get_logger()


def main():
    session = db.connect_db()

    cities = session.query(City)

    for city in cities:
        # country = session.query(Country).filter_by(id=city.id_country).one()
        # LOGGER.info('1 %s %s', country.name, country.code)

        LOGGER.info('2 %s, %s - %s %s', city.name, city.state, city.country, city.country_code)
        # city.country = country.name
        # city.country_code = country.code
        # LOGGER.info('3 %s, %s - %s %s', city.name, city.state, city.country, city.country_code)
        # session.commit()


if __name__ == '__main__':
    main()
