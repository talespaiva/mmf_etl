import os
import sys
sys.path.insert(0, os.path.pardir)

from itertools import zip_longest

import numpy as np
from shapely import wkt
from geopy.distance import vincenty

import log
from exceptions import TrajectoryTooShortException

LOGGER = log.get_logger()


def get_distances(trajectory):
    """
    Retrieves the distances between each of the trajectory points
    """
    lengths = []
    for point, next_point in zip(trajectory.coords, trajectory.coords[1:]):
        dist = vincenty((point[1], point[0]), (next_point[1], next_point[0])).meters
        lengths.append(dist)

    return lengths


def get_total_distance(trajectory):
    """
    Calculates the length of a trajectory
    """
    return sum(get_distances(trajectory))


def remove_redundant_points(trajectory):
    """
        Removes redundant points in the
        trajectory (zero length between points)

        Parameters
        ----------
        trajectory: WKT trajectory

        Returns
        -------
        WKT trajectory
    """

    lengths = get_distances(trajectory)

    new_coords = []
    discarded = 0
    for point, dist in zip_longest(trajectory.coords, lengths):
        if dist != 0:
            new_coords.append(point)
        else:
            discarded += 1

    if discarded > 0:
        LOGGER.info('%s points discarded.', discarded)

    wkt_coords = [f'{c[0]} {c[1]} {c[2]}' for c in new_coords]

    if len(wkt_coords) <= 1:
        LOGGER.info('Trajectory with 1 or 0 points.')
        raise TrajectoryTooShortException('Trajectory with less than 2 points.')

    return wkt.loads(f'LINESTRING({",".join(wkt_coords)})')


def mad(x):
    return 1.483 * np.median(np.abs(x - np.median(x)))


def modified_zscore(x):
    return 0.6745 * (x - np.median(x)) / mad(x)


def get_outliers(series, threshold=3.5):
    mz = modified_zscore(series)
    low_outliers = [i for i, mz in enumerate(mz) if mz < -threshold]
    high_outliers = [i for i, mz in enumerate(mz) if mz > threshold]

    return (low_outliers, high_outliers)


def trim_long_distances(trajectory, ratio=0.1, min_distance=50):
    """
    Searches for long distances between points
    towards the beginning or end of trajectory.

    Parameters
    ----------
    trajectory: WKT object
        Trajectory to be trimmed

    ratio: float
        Proportion of the trajectory to be considered
        Range: 0.0 - 1.0
        Example: ratio=0.1 means that the first 10% and
        last 10% of the trajectory are considered to be trimmed

    min_distance: int
        Minimum distance (in meters) for a segment to be
        considered as an outlier

    Returns
    -------
    Trimmed WKT trajectory
    """

    distances = get_distances(trajectory)

    # 1 - identify the long distances
    _, outliers = get_outliers(distances)

    # 2 - locate outlier positions relative to the trajectory
    outlier_positions = [i / len(distances) for i in outliers]
    outlier_distances = [distances[i] for i in outliers]

    # 3 - select the ones that are either close to the begin 
    # or to the end
    near_start = [
        outliers[i] for i, out in enumerate(outlier_positions)
        if out <= ratio and outlier_distances[i] > min_distance
    ]
    near_end = [
        outliers[i] for i, out in enumerate(outlier_positions)
        if out >= 1 - ratio and outlier_distances[i] > min_distance
    ]

    # 4 - rebuild trimmed trajectory
    start, end = 0, len(trajectory.coords)

    if near_start:
        start = max(near_start) + 1
    if near_end:
        end = min(near_end)

    wkt_coords = [f'{c[0]} {c[1]} {c[2]}' for c in trajectory.coords[start:end]]

    trimmed_points = len(trajectory.coords) - len(wkt_coords)
    if trimmed_points > 0:
        LOGGER.info('%s point(s) removed', trimmed_points)

    if len(wkt_coords) <= 1:
        LOGGER.info('Trajectory with 1 or 0 points.')
        raise TrajectoryTooShortException('Trajectory with less than 2 points.')

    return wkt.loads(f'LINESTRING({",".join(wkt_coords)})')
