import os
import sys
sys.path.insert(0, os.path.pardir)

from shapely import wkt
from geopy.distance import vincenty

import log
import db
from database import preprocessing as pp
from models import Workout, User

LOGGER = log.get_logger()


def main():
    all_trimmed_points_count = 0
    session = db.connect_db()
    user_query = session.query(User)

    for user in user_query:
        LOGGER.info('User %s', user.id)
        workouts_query = session.query(Workout).filter_by(id_user=user.id)

        for workout in workouts_query:
            LOGGER.info('Workout %s', workout.id)

            raw_trajectory = wkt.loads(session.scalar(workout.raw_trajectory.ST_AsText()))
            trajectory = pp.remove_redundant_points(raw_trajectory)

            lengths = pp.get_distances(trajectory)

            # 1 - identify the long distances
            _, outliers = pp.get_outliers(lengths)

            # 2 - locate outlier positions relative to the trajectory
            outlier_positions = [i / len(lengths) for i in outliers]
            outlier_distances = [lengths[i] for i in outliers]

            # 3 - select the ones that are either close to the begin (before 10% of
            # the total distance) or the end (after 90% of the total distance)
            near_start = [
                outliers[i] for i, out in enumerate(outlier_positions)
                if out <= 0.1 and outlier_distances[i] > 50
            ]
            near_end = [
                outliers[i] for i, out in enumerate(outlier_positions)
                if out >= 0.9 and outlier_distances[i] > 50
            ]

            # 4 - rebuild trimmed trajectory
            if near_start or near_end:
                start, end = 0, len(lengths)

                if near_start:
                    start = max(near_start) + 1
                if near_end:
                    end = min(near_end)

                wkt_coords = [f'{c[0]} {c[1]} {c[2]}' for c in trajectory.coords[start:end]]

                trimmed_points_count = len(raw_trajectory.coords) - len(
                    trajectory.coords[start:end])
                if trimmed_points_count > 0:
                    LOGGER.info('%s points removed', trimmed_points_count)
                    all_trimmed_points_count += trimmed_points_count

                workout.trajectory = f'LINESTRING({",".join(wkt_coords)})'

        LOGGER.info('Commiting changes...')
        session.commit()

    LOGGER.info('Total points removed: %s', all_trimmed_points_count)
    session.close()


if __name__ == '__main__':
    main()
