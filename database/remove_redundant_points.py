import os
import sys
sys.path.insert(0, os.path.pardir)

from shapely import wkt
from geopy.distance import vincenty

import log
import db
from models import Workout, User

LOGGER = log.get_logger()


def main():
    all_discarded, all_kept = 0, 0
    session = db.connect_db()
    user_query = session.query(User)

    for user in user_query:
        LOGGER.info('User %s', user.id)
        workouts_query = session.query(Workout).filter_by(id_user=user.id)

        for workout in workouts_query:
            LOGGER.info('\tWorkout %s', workout.id)

            trajectory = wkt.loads(session.scalar(workout.temptraj.ST_AsText()))

            # remove redundant points in the trajectory (zero length between points)
            lens = [-1]
            for point, next_point in zip(trajectory.coords, trajectory.coords[1:]):
                dist = vincenty((point[1], point[0]), (next_point[1], next_point[0])).meters
                lens.append(dist)

            new_coords = []
            discarded = []
            for point, dist in zip(trajectory.coords, lens):
                if dist != 0:
                    new_coords.append(point)
                else:
                    discarded.append(dist)

            all_discarded += len(discarded)
            all_kept += len(new_coords)

            LOGGER.info('%s points kept, %s points discarded (%s)',
                        len(new_coords), len(discarded), workout.source)

            wkt_coords = [f'{c[0]} {c[1]} {c[2]}' for c in new_coords]

            if len(wkt_coords) < 2:
                LOGGER.info('NOT SAVING: len(new trajectory) < 2')
                continue
            workout.temptraj = f'LINESTRINGZ({",".join(wkt_coords)})'

        LOGGER.info('Commiting changes...')
        session.commit()

    LOGGER.info('total kept: %s', all_kept)
    LOGGER.info('total discarded: %s', all_discarded)
    session.close()


if __name__ == '__main__':
    main()
