"""
    This script applies basic cleaning
    procedures to trajectories: it removes
    redundant points and then removes outliers
    at the head and tail of each trajectory.

    It also updates the startting address,
    distance, and duration.
"""

import os
import sys
sys.path.insert(0, os.path.pardir)

from shapely import wkt

import log
import db
from models import Workout, User
from database import preprocessing as pp
import exceptions
import reverse_geocode

LOGGER = log.get_logger()


def main():
    session = db.connect_db()
    user_query = session.query(User)

    for user in user_query:
        LOGGER.info('User %s', user.id)
        workouts_query = session.query(Workout).filter_by(id_user=user.id)

        for workout in workouts_query:
            if workout.trajectory is not None:
                continue

            LOGGER.info('Workout %s', workout.id)

            raw_trajectory = wkt.loads(session.scalar(workout.raw_trajectory.ST_AsText()))

            trajectory = None
            try:
                trajectory = pp.remove_redundant_points(raw_trajectory)
                trajectory = pp.trim_long_distances(trajectory, 0.05, 50)
            except exceptions.TrajectoryTooShortException:
                trajectory = raw_trajectory

            LOGGER.info('%s | %s', len(raw_trajectory.coords), len(trajectory.coords))

            workout.trajectory = str(trajectory)
            workout.distance = pp.get_total_distance(trajectory)

            #city_id, raw_location = reverse_geocode.get_city_and_address(trajectory)
            #workout.id_city = city_id
            #workout.start_address = str(raw_location)

            workout.duration = int(trajectory.coords[-1][2]) - int(trajectory.coords[0][2])

            workout.diff_distance = workout.mmf_distance - workout.distance

            #if len(session.dirty) > 0:
            #    LOGGER.info('Commiting changes...')
            #    session.commit()

        if len(session.dirty) > 0:
            LOGGER.info('Commiting changes...')
            session.commit()

    session.close()


if __name__ == '__main__':
    main()
