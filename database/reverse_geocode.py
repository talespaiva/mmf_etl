import os
import sys
sys.path.insert(0, os.path.pardir)

import log
from database import db
from database import models

import time

from shapely import wkt
from geopy.geocoders import Nominatim

LOGGER = log.get_logger()

geolocator = Nominatim(user_agent="mmf")


def get_location(point, max_retries=1000, sleep_time=10):
    for i in range(1, max_retries + 1):
        try:
            location = geolocator.reverse(point)
            return location
        except Exception as exception:
            if i == max_retries:
                raise
            LOGGER.info(exception)
            LOGGER.info('Request failed (attempt %s). Sleeping for %s seconds', i, sleep_time)
            time.sleep(sleep_time)


def get_city_and_address(trajectory):
    session = db.connect_db()

    first_point = None
    for point in trajectory.coords:
        if point[0] != 0.0 and point[1] != 0.0:
            first_point = f'{point[1]}, {point[0]}'
            break

    location = get_location(first_point)

    city_name = 'Unknown'
    state = 'Unknown'
    country_name = 'Unknown'
    country_code = 'Unknown'

    if 'address' in location.raw:
        if 'town' in location.raw['address']:
            city_name = location.raw['address']['town']
        elif 'city' in location.raw['address']:
            city_name = location.raw['address']['city']
        elif 'hamlet' in location.raw['address']:
            city_name = location.raw['address']['hamlet']
        elif 'village' in location.raw['address']:
            city_name = location.raw['address']['village']
        elif 'suburb' in location.raw['address']:
            city_name = location.raw['address']['suburb']
        elif 'locality' in location.raw['address']:
            city_name = location.raw['address']['locality']

        if 'state' in location.raw['address']:
            state = location.raw['address']['state']
        elif 'county' in location.raw['address']:
            state = location.raw['address']['county']

        if 'country' in location.raw['address']:
            country_name = location.raw['address']['country']
        if 'country_code' in location.raw['address']:
            country_code = location.raw['address']['country_code']

    city, _ = db.get_one_or_create(
        session,
        models.City,
        'create',
        name=city_name,
        state=state,
        country=country_name,
        country_code=country_code)

    city_id = city.id

    session.close()

    return city_id, location.raw


def main():
    session = db.connect_db()
    user_query = session.query(models.User)

    for user in user_query:
        LOGGER.info('User %s', user.id)
        workouts_query = session.query(models.Workout).filter_by(id_user=user.id, id_city=None)

        for workout in workouts_query:
            LOGGER.info('\tWorkout %s', workout.id)

            if workout.start_address is not None:
                LOGGER.info('Skipping...')
                continue

            trajectory = wkt.loads(session.scalar(workout.trajectory.ST_AsText()))
            first_point = None
            for point in trajectory.coords:
                if point[0] != 0.0 and point[1] != 0.0:
                    first_point = f'{point[1]}, {point[0]}'
                    break

            LOGGER.info(first_point)

            location = get_location(first_point)
            LOGGER.info(location.raw)

            city_name = 'Unknown'
            state = 'Unknown'
            country_name = 'Unknown'
            country_code = 'Unknown'
            if 'address' in location.raw:
                if 'town' in location.raw['address']:
                    city_name = location.raw['address']['town']
                elif 'city' in location.raw['address']:
                    city_name = location.raw['address']['city']
                elif 'hamlet' in location.raw['address']:
                    city_name = location.raw['address']['hamlet']
                elif 'village' in location.raw['address']:
                    city_name = location.raw['address']['village']
                elif 'suburb' in location.raw['address']:
                    city_name = location.raw['address']['suburb']
                elif 'locality' in location.raw['address']:
                    city_name = location.raw['address']['locality']

                if 'state' in location.raw['address']:
                    state = location.raw['address']['state']
                elif 'county' in location.raw['address']:
                    state = location.raw['address']['county']

                country_name = location.raw['address']['country']
                country_code = location.raw['address']['country_code']

            LOGGER.info('%s | %s | %s | %s', city_name, state, country_name, country_code)

            city, _ = db.get_one_or_create(
                session,
                models.City,
                'create',
                name=city_name,
                state=state,
                country=country_name,
                country_code=country_code)

            workout.id_city = city.id
            workout.start_address = str(location.raw)

            time.sleep(3)
            session.commit()

    session.close()


if __name__ == '__main__':
    main()
