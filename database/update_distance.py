import os
import sys
sys.path.insert(0, os.path.pardir)

from shapely import wkt

import log
import db
from models import Workout, User
from database import preprocessing as pp

LOGGER = log.get_logger()


def main():
    session = db.connect_db()
    user_query = session.query(User)

    for user in user_query:
        #LOGGER.info('User %s', user.id)
        workouts_query = session.query(Workout).filter_by(id_user=user.id)

        for workout in workouts_query:
            #LOGGER.info('Workout %s', workout.id)

            trajectory = wkt.loads(session.scalar(workout.trajectory.ST_AsText()))
            new_distance = pp.get_total_distance(trajectory)
            if new_distance != trajectory.distance:
                LOGGER.info('%s/%s old: %s, new: %s', user.id, workout.id, workout.distance, new_distance)
                
            workout.distance = new_distance

        LOGGER.info('Commiting changes...')
        session.commit()

    session.close()


if __name__ == '__main__':
    main()
