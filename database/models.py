import os
import sys
sys.path.insert(0, os.path.pardir)

from datetime import datetime, timedelta

from shapely import wkt
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Float
from sqlalchemy.orm import relationship
from geoalchemy2 import Geography

import log
from database import preprocessing as pp
from database import reverse_geocode
from mmf import mapmyfitness as mmf
from exceptions import TrajectoryTooShortException, TrajectoryTooLongException

LOGGER = log.get_logger()
DISTANCE_THRESHOLD = 45_000  # in meters

Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(String, primary_key=True)
    username = Column(String)
    display_name = Column(String)
    gender = Column(String)
    country = Column(String)
    locality = Column(String)
    region = Column(String)

    def create(id=None, username=None, display_name=None, gender=None, location=None, **kwargs):
        if location:
            country = location['country']
            locality = location['locality']
            region = location['region']
        else:
            country, locality, region = None, None, None

        new_user = User(
            id=id,
            username=username,
            display_name=display_name,
            gender=gender,
            country=country,
            locality=locality,
            region=region)

        return new_user


class Workout(Base):
    __tablename__ = 'workout'
    id = Column(String, primary_key=True)
    id_user = Column(String, ForeignKey('user.id'))
    id_city = Column(String, ForeignKey('city.id'))
    activity_id = Column(Integer)
    subactivity_id = Column(Integer)
    start_datetime = Column(DateTime)
    source = Column(String)
    mmf_distance = Column(Float)
    mmf_duration = Column(Integer)
    start_address = Column(String)
    distance = Column(Float)
    duration = Column(Integer)
    diff_distance = Column(Float)
    raw_trajectory = Column(Geography('LINESTRINGZ', dimension=3))
    trajectory = Column(Geography('LINESTRINGZ', dimension=3))

    user = relationship("User", foreign_keys=[id_user])
    city = relationship("City", foreign_keys=[id_city])

    def create(id=None,
               id_user=None,
               activity_id=None,
               subactivity_id=None,
               start_datetime=None,
               source=None,
               mmf_distance=None,
               distance=None,
               mmf_duration=None,
               duration=None,
               diff_distance=None,
               raw_trajectory=None,
               trajectory=None,
               _links=None,
               aggregates=None,
               time_series=None,
               created_datetime=None,
               **kwargs):

        workout_id = _links['self'][0]['id']
        user_id = _links['user'][0]['id']
        subactivity_id = _links['activity_type'][0]['id']
        activity_id = mmf.get_root_activity(subactivity_id)

        mmf_distance, mmf_duration = None, None
        if aggregates:
            if 'distance_total' in aggregates:
                mmf_distance = aggregates['distance_total']
            if 'elapsed_time_total' in aggregates:
                mmf_duration = aggregates['elapsed_time_total']

        if time_series and 'position' in time_series:
            point_list = time_series['position']

            if len(point_list) <= 1:
                LOGGER.info('Trajectory with 1 or 0 points. Skipping...')
                raise TrajectoryTooShortException('Trajectory with 1 or 0 points.')

            start_time = datetime.strptime(created_datetime.split('+')[0], "%Y-%m-%dT%H:%M:%S")
            wkt_points = []

            for time_offset, point in point_list:
                lat = point['lat']
                lng = point['lng']

                delta = timedelta(seconds=float(time_offset))
                _time = (start_time + delta)
                timestamp = (_time - datetime(1970, 1, 1)) // timedelta(seconds=1)

                wkt_points.append(f'{lng} {lat} {timestamp}')

            raw_trajectory = wkt.loads('LINESTRING({0})'.format(','.join(wkt_points)))

            # Data cleaning procedures
            trajectory = pp.remove_redundant_points(raw_trajectory)
            trajectory = pp.trim_long_distances(trajectory, 0.05, 50)

            if len(trajectory.coords) < 2:
                raise TrajectoryTooShortException('Trajectory with less than 2 points.')

            distance = pp.get_total_distance(trajectory)
            if distance > DISTANCE_THRESHOLD:
                raise TrajectoryTooLongException(f'Trajectory too long. \
                                {distance} meters > {DISTANCE_THRESHOLD} meters')

            diff_distance = mmf_distance - distance

            ### duration = int(trajectory.coords[-1][2]) - int(trajectory.coords[0][2])

            city_id, raw_location = reverse_geocode.get_city_and_address(raw_trajectory)
            raw_location = str(raw_location)

        new_workout = Workout(
            id=workout_id,
            id_user=user_id,
            id_city=city_id,
            activity_id=activity_id,
            subactivity_id=subactivity_id,
            start_datetime=start_datetime,
            source=source,
            mmf_distance=mmf_distance,
            distance=distance,
            diff_distance=diff_distance,
            mmf_duration=mmf_duration,
            duration=duration,
            raw_trajectory=str(raw_trajectory),
            trajectory=str(trajectory),
            start_address=raw_location)

        return new_workout


class City(Base):
    __tablename__ = 'city'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    state = Column(String)
    country = Column(String)
    country_code = Column(String)

    def create(name=None, state=state, country=country, country_code=country_code, **kwargs):

        new_city = City(name=name, state=state, country=country, country_code=country_code)

        return new_city
